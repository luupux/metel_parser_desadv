#!/usr/bin/python
# -*- coding: utf-8 -*-
#:W"
''' 
CREO FILE ZIP DI DIMENSIONE DEFINITA PARTENDO DA UN FILE
CHE CONTIENE L'ELENCO DEI FILE DA ZIPPARE
 
 CREATE TABLE `logstampe` (
	`STAMPA`	INTEGER,
	`NOME_FISICO2`	TEXT,
	`DATA_RICH`	NUMERIC,
	`ORA_RICH`	REAL,
	`ELABORATO`	INTEGER DEFAULT 0,
	PRIMARY KEY(STAMPA,NOME_FISICO2,DATA_RICH,ORA_RICH)
);
'''
''' NOTE
CREATE TABLE 'ERRORI_DESADV' (
	'FILENAME' TEXT ,
	'AVVISO'	TEXT,
	'DATA-AV'	TEXT,
	'FORNITORE'	TEXT,
	'PRECODICE'	TEXT,
	'CODICE-ART'	TEXT,
	'DESCRIZIONE'	TEXT,
	'REPARTO'	TEXT,
	'CAUSALE'	TEXT,
	'ANNO_ORDINE'	TEXT,
	'NUM_ORDINE'	TEXT,
	'DATA_ORDINE'	TEXT,
	'QTA'	TEXT,
	'NOTE'	TEXT,
	'LINK_CORPO' TEXT,
	'ORIGINALE' TEXT,
	'NUM_RIGA' TEXT,
  	'PROG_RIGA' TEXT,
	'QTA_CONS'  TEXT,
	'RAG_SOC'   TEXT,
	'ISOLA'   TEXT,
	'ELABORATO'	INTEGER DEFAULT 0	)'''
	
import pprint
import os
import string
import re
import zipfile
import datetime
import shutil
import logging
import glob
import time
from string import rstrip
import ibm_db
from time import gmtime, strftime
from simpledbf import Dbf5
import sqlite3 
import csv
logging.basicConfig(filename='error.log',level=logging.DEBUG,format='%(levelname)s %(asctime)s %(message)s',datefmt='%m/%d/%Y %I:%M:%S %p')
logging.info(" --# START EXECUTION #--")

class manageMsg:
	def __init__ (self):
		""" CLASSE DI CREAZIONE MESSAGGI DI ERRORI"""
		self.reportfile = 'C:\\DEV\\Varie\\METEL_PARSER\\LOGELABORAZIONI\\report-%s.txt' % datetime.date.today()
		self.target = open(self.reportfile, 'a')
		self.msgdefault ="""
		#################################### --- DEFINIZIONE DEGLI ERRORI --- #######################################
		** ORDINE GIA` EVASO **		|Flusso elettronico e stato ricevuto dopo l'effettiva consegna della merce
		ARTICOLO INESISTENTE		|Ean  indicado nel flusso e'  errato
		** ARTICOLO NON IN ORDINE **	|L'articolo indicato nel flusso non risulta presente nell' ordine confermato
		GLN ERRATO			|Il gln indicato e' errato o il N.ordine non fa riferimento a quel GLN
		Formato ord.euritmo		|Il numero d'ordine indicato e' indicato in modo errato
		Ordine non trovato per rep ecc.	|Il numero d'ordine indicato non e' riferito al gln (pdv) indicato 
		Il GLN  non risulta associato	|Il gln errato
		##############################################################################################################
			"""
	
		sqlSel="""select * from ERRORI_DESADV WHERE ELABORATO = 0  ORDER BY  RAG_SOC, NOTE, "DATA-AV" """
		#sqlSel="""select * from ERRORI_DESADV WHERE ELABORATO = 0  and rag_soc='ARGOCLIMA SPA' ORDER BY  RAG_SOC  ,"DATA-AV", AVVISO"""
		self.conlite = sqlite3.connect('logstampe.db')
		self.curlite = self.conlite.cursor()  
		rows = self.curlite.execute(sqlSel)
		rowdict ={}
		elencoForn = {}

		testforn ='' 
		testmsg =''
		self.msgdict = {}
		msgnote ={}
		cf = 0
		msgroup = {}
		for row in rows :
			#print row
			rowdict['AVVISO'] = row[1]
			rowdict['DATAAV'] = row[2]
			rowdict['FORNITORE'] = row[3]
			rowdict['PRECODICE'] = row[4]
			rowdict['CODICEART'] = row[5]
			rowdict['DESCRIZIONE'] = row[6]
			rowdict['REPARTO'] = row[7]
			rowdict['CAUSALE'] = row[8]
			rowdict['ANNOORDINE'] = row[9]
			rowdict['NUMORDINE'] = row[10]
			rowdict['DATAORDINE'] = row[11]
			rowdict['QTA'] = row[12]
			rowdict['NOTE']  = row[13]
			rowdict['CODORIGINALE'] = row[15]
			rowdict['RAGSOC'] = row[19]

			if rowdict['RAGSOC'] <>  testforn : # TEST SE UN NUOVO FORNITORE
				if testforn != '' : # TEST SE E' SETTATO IL FORNITORE	
					print "A-- %s -- %s " % (testforn,rowdict['RAGSOC'])
					print "testforn %s" % testforn
				testforn = rowdict['RAGSOC']
				self.msgdict.setdefault(testforn,[])
				print "B-- %s " % testforn
			print "	DETAIL TO MSG %s --- %s -- %s " % (testforn,rowdict['NOTE'], 1)
			self.msgdict[testforn].append(row)
	def  allmsg(self,format=0):
		# format = 1 " se e valorizzato  cambia la visualizzazione del report da 1 colonna a multi colonne"
		self.target.write("\nINIZIO ELABORAZIONE DEL "+str(datetime.datetime.now())+"\n")
		for al in self.msgdict:
			if format <> 0:
				self.msgforn(al)
			else:
				self.msgforn1col(al)
			
		self.target.write("FINE ELABORAZIONE DEL "+str(datetime.datetime.now())+"\n")
		self.target.close()

	def msgforn(self,nameforn):
		""" VISUALIZZA TUTTI I MESSAGI DI UNO SPECIFICO FORNITORE """
		print "MSG FORN %s " % nameforn
		testata = "%-40s|%-20s|%-20s|%-20s|%-13s|%-15s|%-20s|%-15s|%-20s|%-30s| "  % ('MESSAGGIO DI SCARTO','FORNITORE','AVVISO','DATA AVVISO','N.ORD.','DATA ORDINE','C.ORIG','PRE','CODICE','DESCR')
		print testata
		self.target.write(testata+"\n")
		for al in self.msgdict[nameforn]:
			lf = list(al)
			#lf = list(ar[1])
			#print lf
			rigamsg =  "%-40s|%-20s|%-20s|%-20s|%-13s|%-15s|%-20s|%-15s|%-20s|%-30s| " % (str(lf[13])[:40],str(lf[19])[:20], str(lf[1])[:30], str(lf[2])[:10], str(lf[10])[:13],str(lf[11])[:10],str(lf[15])[:20],str(lf[4]),str(lf[5])[:20],str(lf[6])[:30] )
			print rigamsg
			self.target.write(rigamsg+"\n")
		print self.msgdefault
		print "\n"
		self.target.write("\n")
		
	def msgforn1col(self,nameforn):
		""" VISUALIZZA TUTTI I MESSAGI DI UNO SPECIFICO FORNITORE  su una solo COLONNA"""
		msgtesta =  "####  %s  ####" % nameforn
		print msgtesta
		self.target.write(msgtesta+"\n")
		#self.target.write(testata+"\n")
		for al in self.msgdict[nameforn]:
			lf = list(al)
			#lf = list(ar[1])
			#print lf
			#('MESSAGGIO DI SCARTO','FORNITORE','AVVISO','DATA AVVISO','N.ORD.','DATA ORDINE','C.ORIG','PRE','CODICE','DESCR')
			#rigamsg =  "%-40s|%-20s|%-20s|%-20s|%-13s|%-15s|%-20s|%-15s|%-20s|%-30s| " % (str(lf[13])[:40],str(lf[19])[:20], str(lf[1])[:30], str(lf[2])[:10], str(lf[10])[:13],str(lf[11])[:10],str(lf[15])[:20],str(lf[4]),str(lf[5])[:20],str(lf[6])[:30] )
			rigamsg = "%-13s %s %-13s %s %s %s %-13s %s %-13s %s %-13s %s %-13s %s %-13s %s %-13s %s\n\n"  % ( \
					"ERRORE", str(lf[13]), \
					"\nAVVISO N. ", str(lf[1])[:30],  \
					"DEL ",str(lf[2]), \
					"\nN.ORDINE.",str(lf[10])[:10], \
					"\nDATA ORDINE",str(lf[11])[:13],\
					"\nC.ORIGINALE",str(lf[15])[:10], \
					"\nPRECODICE",str(lf[4])[:20], \
					"\nCODICE",str(lf[5]), \
					"\nDESCRIZIONE",str(lf[6]), 
					)

			print rigamsg
			self.target.write(rigamsg+"\n")
		print self.msgdefault
		print "\n"
		self.target.write("\n")
		
#a = manageMsg()
#pr = a.allmsg()
#exit(0)	
	
#################################
class logStampe:
	"""LEGGO I DATI SULLE STAMPE DA DB2 E LE INSERISCO NEL DB DI STAMPE PER L'ELABORAZIONE SUCCESSIVA"""
	def __init__ (self):
		self.conn = ibm_db.connect('DATABASE=XVM;HOSTNAME=172.16.10.120;PORT=50000;PROTOCOL=TCPIP;UID=xvmodbc;PWD=xvmodbc;','','')
		self.stmt = ''
		self.sqlID="""SELECT NOME_FISICO2,DATA_RICH,ORA_RICH,STAMPA from D01.STAMPE
					 WHERE oggetto = 'P32718' AND utente = 'VMcron'  
					AND DATA_RICH  >  CURRENT DATE  - 2 DAYS
					ORDER BY DATA_RICH DESC;"""
		self.conlite = sqlite3.connect('logstampe.db')
		self.curlite = self.conlite.cursor()  
		al = self.curlite.execute('SELECT * from  logstampe')
		'''
		for row in al:
			print row
		'''
	def popLogStampe(self):
		self.stmt = ibm_db.exec_immediate(self.conn, self.sqlID)
		self.dictD = ibm_db.fetch_assoc(self.stmt)
		while self.dictD != False:
			self.NOME_FISICO2 = str(self.dictD['NOME_FISICO2']).strip()
			self.DATA_RICH = str(self.dictD['DATA_RICH']).strip()
			self.ORA_RICH = str(self.dictD['ORA_RICH']).strip()
			self.STAMPA = str(self.dictD['STAMPA']).strip()
			'''
			print "NOME_FISICO2 : ", self.NOME_FISICO2
			print "DATA_RICH	 : ", self.DATA_RICH
			print "ORA_RICH 	 : ", self.ORA_RICH
			print "STAMPA 		 : ", self.STAMPA
			print  "------"
			'''
			var1 = "'"+self.STAMPA+"','"+self.NOME_FISICO2+"','"+self.DATA_RICH+"','"+self.ORA_RICH+"'"
			#print var1
			var  = " INSERT OR IGNORE INTO logstampe(`STAMPA`,`NOME_FISICO2`,`DATA_RICH`,`ORA_RICH`) VALUES ( %s )"  % var1 
			#print var
			self.curlite.execute(var)
			self.conlite.commit()
			self.dictD = ibm_db.fetch_assoc(self.stmt) 
#a = logStampe()
#a.popLogStampe()

##################



class convert_dbf:
	def __init__(self):
		self.path_sourcedbf = path_sourcedbf

	def dbf_2_csv(self,dbfname):
		self.dbfname = dbfname.encode('UTF8')
		#print self.dbfname
		dbfpot = Dbf5(self.path_sourcedbf+self.dbfname, codec='utf-8')
		if os.path.isfile('temp.csv'): 
			shutil.move('temp.csv','temp-old.csv')
		try:
			#dbfpot.to_csv(self.dbfname+'.csv')
			#csvstampe = csv.reader(open(self.dbfname+'.csv'))
			dbfpot.to_csv('temp.csv')
			csvstampe = csv.reader(x.replace('\0', '') for x in open('temp.csv'))
			print "\n\nPROSSIMA ELABORAZIONE  --- %s  "% (self.dbfname)
			reader_set = 0
			csvstampe.next()
			head_row = """ INSERT INTO `ERRORI_DESADV` (`FILENAME`,`AVVISO`,`DATA-AV`,`FORNITORE`,`PRECODICE`,`CODICE-ART`,`DESCRIZIONE`,`REPARTO`,`CAUSALE`,`ANNO_ORDINE`,`NUM_ORDINE`,`DATA_ORDINE`,`QTA`,`NOTE`,`LINK_CORPO`,`ORIGINALE`,`NUM_RIGA`,`PROG_RIGA`,`QTA_CONS`,`RAG_SOC`,`ISOLA`) VALUES ( """
			for row in csvstampe:
				#print row
				#print row[0]
				strong = [ "\""+x.strip("\"")+"\""  for  x in row]
				insert_row = "%s  '%s', %s) " %(head_row,self.dbfname,",".join(strong))
				#print insert_row
				conn = sqlite3.connect('logstampe.db')
				cursor = conn.cursor()
				cursor.execute(insert_row)
				conn.commit()
		except Exception as e:
			print "ERROR  FILE=%s  MSG=%s"  % (self.dbfname , e)
			errmsg="DBF2CSV  FAILED :  FILE= %s  MSG= %s"  % (self.dbfname , e)
			logging.warning(errmsg)



######################
#time_check =  strftime("%Y-%m-%d", gmtime())
time_check = '2015-07-23'
path_sourcedbf = "\\\\172.16.10.120\\spool\\"
#path_sourcedbf = "C:\\DEV\\Varie\\METEL_PARSER\\STOR-DBFRESULT\\"

#list_dbf =[]
#where_clausole = ''
## ESTRAGGO LA LISTA DELLE STAMPE DA STRIKE
r = logStampe()
r.popLogStampe()
#############

## ELABORO I FILE DBF 
a = convert_dbf()
conn = sqlite3.connect('logstampe.db')
sqldaElaborare = "SELECT * FROM logstampe where  ELABORATO = 0  AND NOME_FISICO2 IS NOT NULL ORDER BY DATA_RICH DESC"
cursor = conn.cursor()
cursor.execute(sqldaElaborare)
rows = cursor.fetchall()
print "DOCUMENTI DA ELABORARE %s " % len(rows)
# PREPARO I MESSAGGI  DI TUTTI LE RIGE DI ERRORI_DESADV CON ELABORATO =0

""" AGGIORNO LA TABELLA LOGSTAMPE IMPOSTANDO IMPOSTATO ELABORATO = 1 
	COSI DA AVERE TRACCIA TRA QUELLO CHE E' STATO GIA PRELEVATO DALLE STAMPE STRIKE
	E QUELLO CHE RIMANE DA PROCESSARE."""
for row in rows:
	print row
	update_status  = "STAMPA='"+str(row[0])+"' AND NOME_FISICO2='"+str(row[1])+"' AND  DATA_RICH='"+str(row[2])+"'AND ORA_RICH='"+str(row[3])+"'"
	update_row = "update logstampe set ELABORATO = 1 WHERE "+update_status
	cursor.execute(update_row)
	conn.commit()
	#print "CONVERTO IL FILE %s " % row[1]
	a.dbf_2_csv(row[1])

""" PREPARO I MESSAGGI PER LE SEGNALAZIONI FORNITORI """
parp = manageMsg()
pr = parp.allmsg()
""" SEGNO COME PROCESSATO I MESSAGGI CHE SONO STATI GIA' ELABORATO PER L'INVIO """
#update_errori_desadv = "update ERRORI_DESADV SET ELABORATO = 1"
#cursor.execute(update_errori_desadv)
conn.commit()
logging.info(" --# END EXCECUTION #--")
	
	

